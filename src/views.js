import { writable } from 'svelte/store';

export const eventView = writable(true);
export const eventData = writable([]);
export const taskView = writable(true);
export const taskData = writable([]);
export const clientView = writable(true);
export const clientsData = writable([]);
export const employeeView = writable(true);
export const employeeData = writable([]);
export const budgetrequestView = writable(true);
export const budgetrequestData = writable([]);
